package gor.samp.sockettest.android;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.net.Socket;
import android.support.v7.app.ActionBarActivity;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

	private static final String balls = "BAAAAALLS";
	private static final String SAVE_DATE = "saveDate484545451";
	private static final String SAVE_CAPITAL = "saveCapital48451";
	private final String serverAddress = "192.168.1.8";

	private boolean run = false;
	private boolean runChat = false, runChatSend = false;
	private boolean chatNameAccepted = false;
	private String username;
	Button buttonDate, buttonCapital;
	EditText editChat;
	Object capitalLock = new Object();
	private SocketChatTask chatTask;
	private Socket clientSocketChat;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.activity_main);
		buttonDate = (Button)findViewById(R.id.title_button);
		buttonCapital = (Button)findViewById(R.id.capital_button);
		if(bundle!=null){
			buttonDate.setText(bundle.getCharSequence(SAVE_DATE));
			buttonCapital.setText(bundle.getCharSequence(SAVE_CAPITAL));
		}
		editChat = (EditText)findViewById(R.id.chat_edit_text);
		editChat.setImeOptions(EditorInfo.IME_ACTION_DONE);
		editChat.setOnClickListener(new ChatClickListener());
		editChat.setOnKeyListener(new ChatEditListener());
	}

	protected void onSaveInstanceState(Bundle bundle){
		bundle.putCharSequence(SAVE_DATE, buttonDate.getText());
		bundle.putCharSequence(SAVE_CAPITAL, buttonCapital.getText());
		super.onSaveInstanceState(bundle);
	}

	// date button
	public void onClick_Date(View v){
		if(!run){
			run = true;
			SocketDateTask socketTask = new SocketDateTask(buttonDate);
			socketTask.execute();
		}
	}

	// capital button
	public void onClick_Capital(View v){
		if(!run){
			getCapital();
		}
	}

	// capitalize dialog
	private void getCapital(){
		final EditText editTextCapital = new EditText(this);
		editTextCapital.setImeOptions(EditorInfo.IME_ACTION_DONE);
		editTextCapital.setOnKeyListener(new OnKeyListener(){

			@Override
			public boolean onKey(View arg0, int arg1, KeyEvent keyEvent) {
				if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_ENTER){
					run = true;
					SocketCapitalTask socketTask = new SocketCapitalTask(buttonCapital);
					socketTask.execute(editTextCapital.getText().toString());
				}
				return false;
			}});
		new AlertDialog.Builder(MainActivity.this)
		.setTitle("Capitalize")
		.setMessage("Enter something to capitalize")
		.setView(editTextCapital)
		.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				run = true;
				SocketCapitalTask socketTask = new SocketCapitalTask(buttonCapital);
				socketTask.execute(editTextCapital.getText().toString());
				dialog.dismiss();
			}
		}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				dialog.dismiss();
			}
		}).show();
	}

	// chat username dialog
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void getUsername(){
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB){
			if(chatNameAccepted && username!=null){
				chatTask = new SocketChatTask();
				chatTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, username);
			}
			final EditText editTextChatUsername = new EditText(this);
			editTextChatUsername.setImeOptions(EditorInfo.IME_ACTION_DONE);
			editTextChatUsername.setOnKeyListener(new OnKeyListener(){

				@Override
				public boolean onKey(View arg0, int arg1, KeyEvent keyEvent) {
					if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_ENTER){
						if(!runChat && username!=null){
							chatTask = new SocketChatTask();
							chatTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, editTextChatUsername.getText().toString());
							return true;
						}
					}
					return false;
				}});

			new AlertDialog.Builder(MainActivity.this)
			.setTitle("Chat Talk")
			.setMessage("Enter username")
			.setView(editTextChatUsername)
			.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					if(!runChat){
						chatTask = new SocketChatTask();
						chatTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, editTextChatUsername.getText().toString());
					}
					dialog.dismiss();
				}
			}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					dialog.dismiss();
				}
			}).show();
		}
	}

	// chat edit text click
	class ChatClickListener implements OnClickListener{

		@Override
		public void onClick(View view) {
			if(!runChat){
				getUsername();
			}
		}
	}

	// chat edit text key
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	class ChatEditListener implements OnKeyListener{

		@Override
		public boolean onKey(View view, int keycode, KeyEvent keyEvent) {
			Log.e(balls, "key event: "+keycode+" enter: "+KeyEvent.KEYCODE_ENTER+" action: "+keyEvent.getAction()+" down:"+KeyEvent.ACTION_DOWN);
			if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB){
				if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_ENTER){
					if(keyEvent.getAction()==KeyEvent.ACTION_DOWN){				
						// send message to socket
						if(!runChatSend && view instanceof EditText){
							String chatMessage = ((EditText)view).getText().toString();
							Log.e(balls, "sending message: "+chatMessage);
							SocketChatSendTask chatSendTask = new SocketChatSendTask();
							chatSendTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, chatMessage);
							return true;
						}
					} else if(keyEvent.getAction()==KeyEvent.ACTION_UP){
						if(view instanceof EditText){
							((EditText)view).setText(null);
						}
					}
				}
			}
			return false;
		}

	}

	class SocketDateTask extends AsyncTask<Void, String, Void>{

		private WeakReference<Button> theButton;

		public SocketDateTask(Button button){
			this.theButton = new WeakReference<Button>(button);
		}

		@Override
		public void onProgressUpdate(String... strings){
			if(strings.length>0){
				Log.e(balls, strings.length+"the string is: "+strings[0]);
				if(this.theButton.get()!=null){	
					this.theButton.get().setText(strings[0]);
				}
			}
		}

		@Override
		protected Void doInBackground(Void... nothing) {
			try{
				Socket clientSocketDate = new Socket(serverAddress, 9090);

				BufferedReader input =
						new BufferedReader(new InputStreamReader(clientSocketDate.getInputStream()));

				while(run){					
					String inputString = input.readLine();
					if(inputString==null){

						run = false;
					} else {
						publishProgress(inputString);
					}
				}

				input.close();
				clientSocketDate.close();

			} catch (Exception e){
				Log.e(balls, "error reading socket", e);
				run = false;
			}
			return null;
		}

		protected void onPostExecute(Void nothing){
			Log.e(balls, "Completed date task");
		}
	}

	class SocketCapitalTask extends AsyncTask<String, String, Void>{

		private WeakReference<Button> theButton;
		private String stringToCapitalize = ".";
		public SocketCapitalTask(Button button){
			this.theButton = new WeakReference<Button>(button);
		}

		@Override
		public void onProgressUpdate(String... strings){
			if(strings.length>0 && theButton.get()!=null){
				if(strings[0].length()>0){
					theButton.get().setText(strings[0]);
				}
				stringToCapitalize = ".";
			}
		}

		@Override
		protected Void doInBackground(String... stringsToCapitalize) {
			try{
				stringToCapitalize=".";
				if(stringsToCapitalize.length>0){
					stringToCapitalize = stringsToCapitalize[0];
				}

				Socket clientSocketCapital = new Socket(serverAddress, 9898);
				BufferedReader input =
						new BufferedReader(new InputStreamReader(clientSocketCapital.getInputStream()));
				PrintWriter out = new PrintWriter(clientSocketCapital.getOutputStream(), true);

				while(run){			
					if(stringToCapitalize!=null && stringToCapitalize.length()>0){
						out.println(stringToCapitalize);
						String inputString = input.readLine();
						if(inputString==null){
							run = false;
						} else {
							publishProgress(inputString);
							Thread.sleep(10);					
						}
					} else {
						run = false;
					}
				}
				out.close();
				input.close();
				clientSocketCapital.close();

			} catch (Exception e){
				Log.e(balls, "error reading socket", e);
				run = false;
			}
			return null;
		}

		protected void onPostExecute(Void nothing){
			Log.e(balls, "Completed capital task");
		}
	}

	class SocketChatTask extends AsyncTask<String, String, Void>{
		public String messageOut = null;
		private WeakReference<TextView> chatTextArea;

		@Override
		protected void onPreExecute(){
			Log.e(balls, "pre execute chat");
			runChat = true;
			chatTextArea = new WeakReference<TextView>((TextView)findViewById(R.id.chat_textarea));
			username = null;
		}

		@Override
		public void onProgressUpdate(String... strings){
			Log.e(balls, "update chat");
			if(strings.length>0){
				chatTextArea.get().append(strings[0]+"\n");
			}
		}

		@Override
		protected Void doInBackground(String... usernames) {
			if(usernames.length>0){
				username = usernames[0];
			}
			Log.e(balls, "chat start socket for: "+username);
			try{
				clientSocketChat = new Socket(serverAddress, 9001);
				BufferedReader chatIn =
						new BufferedReader(new InputStreamReader(clientSocketChat.getInputStream()));
				PrintWriter chatOut = new PrintWriter(clientSocketChat.getOutputStream(), true);
				while(runChat){
					String line;
					if(chatIn.ready()){
						line = chatIn.readLine();
					} else {
						// Log.e(balls, "waiting for input");
						Thread.sleep(100);
						continue;
					}
					Log.e(balls, "line: "+line+" message out: "+messageOut);
					if (line.startsWith("SUBMITNAME")) {
						chatOut.println(username);
					} else if (line.startsWith("NAMEACCEPTED")) {
						chatNameAccepted = true;

					} else if (line.startsWith("MESSAGE")) {
						publishProgress(line.substring(8));
					}
				}
				Log.e(balls, "closing chat socket");
				chatOut.close();
				clientSocketChat.close();
			} catch (Exception e){
				Log.e(balls, "error listening to chat socket", e);
			}
			return null;
		}

		@Override
		public void onPostExecute(Void nothing){
			Log.e(balls, "chat post execute");
			runChat = false;
		}
	}

	class SocketChatSendTask extends AsyncTask<String, String, Void>{
		public String messageOut = null;

		@Override
		protected void onPreExecute(){
			Log.e(balls, "pre execute chat send");
			runChatSend = true;
		}


		@Override
		protected Void doInBackground(String... messages) {
			if(messages.length>0){
				messageOut = messages[0];
			}
			Log.e(balls, "chat send: "+messageOut);
			try{
				if(clientSocketChat!=null){
					PrintWriter chatOut = new PrintWriter(clientSocketChat.getOutputStream(), true);
					if(chatNameAccepted){
						if(messageOut!=null){
							Log.e(balls, "sending chat message out: "+messageOut);
							chatOut.println(messageOut);
						}
					}
				}
			} catch (Exception e){
				Log.e(balls, "error sending message", e);
			}
			return null;
		}

		@Override
		public void onPostExecute(Void nothing){
			Log.e(balls, "chat send post execute");
			runChatSend = false;
		}
	}

}
