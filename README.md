Following the [notes found here](http://cs.lmu.edu/~ray/notes/javanetexamples/), created an Android client to communicate with a Java SocketServer.

-----------------

SERVER: A java project with 4 source files, one for each service

-----------------

Date: this will return the current date in a string format

Capitalize: this takes in a string, capitalizes all the letters, and returns it

MultiChat: this provides a chat system for multiple users

TicTacToe: unimplemented in client

-----------------

CLIENT: Three of the four services can be called on from the Android client app. 

Eclipse Android project. Requires [app_compat_v7 library project](https://developer.android.com/tools/support-library/setup.html#add-library).

-----------------

Date: "Get current date and time" button. press to print time on button

Capitalize: "Get capped" button". on press, user is prompted for string to capitalize, which is then printed on the button

MultiChat: The chat uses the top half of the screen. There is a black area, where text is printed. There is an edit text box that on initial click, prompts the user for a username, and then can be used to send out messages to other users